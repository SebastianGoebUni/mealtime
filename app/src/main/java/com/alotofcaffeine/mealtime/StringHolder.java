package com.alotofcaffeine.mealtime;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.widget.TextView;

public class StringHolder extends ViewHolder {

    public TextView mTextView;

    public StringHolder(TextView itemView) {
        super(itemView);
        mTextView = itemView;
    }
}
