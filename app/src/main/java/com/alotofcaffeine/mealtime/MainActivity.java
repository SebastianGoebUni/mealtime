package com.alotofcaffeine.mealtime;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnSharedPreferenceChangeListener{

    private static final String TAG = MainActivity.class.getSimpleName();
    private TextView mSuggestionTextView;
    private TextView mNewHistoryItemTextView;
    private RecyclerView mRecyclerView;
    private List<String> historyItems;
    private RecyclerView.Adapter<StringHolder> mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        mSuggestionTextView = (TextView) findViewById(R.id.suggestionTextView);
        mNewHistoryItemTextView = (TextView) findViewById(R.id.newHistoryItemTextView);
        mRecyclerView = (RecyclerView) findViewById(R.id.history);

        try {
            JSONArray json = new JSONArray(getSharedPreferences("history", 0).getString("history", "[]"));
            List<String> newHistoryItems = new ArrayList<>();
            for (int i = 0; i < json.length(); i++) {
                newHistoryItems.add(json.optString(i, "I Forgot"));
            }
            this.historyItems = newHistoryItems;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mAdapter = new RecyclerView.Adapter<StringHolder>() {
            @Override
            public StringHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                Log.d(TAG, "onCreateViewHolder");
                TextView v = (TextView) LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.text_view, parent, false);
                return new StringHolder(v);
            }

            @Override
            public void onBindViewHolder(StringHolder holder, int position) {
                Log.d(TAG, "onBindViewHolder");
                holder.mTextView.setText(historyItems.get(position));
            }

            @Override
            public int getItemCount() {
                Log.d(TAG, "item count: " + historyItems.size());
                return historyItems.size();
            }
        };
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        getSharedPreferences("history", 0).registerOnSharedPreferenceChangeListener(this);
    }

    public void newSuggestion(View view) {
        if (historyItems.isEmpty()) {
            mSuggestionTextView.setText("Add some food items first");
            return;
        }
        mSuggestionTextView.setText(historyItems.get((int) (Math.random() * historyItems.size())));
    }

    public void newHistoryItem(View view) {
        if (mNewHistoryItemTextView.getText().toString().trim().equals("")) {
            return;
        }
        SharedPreferences sharedPreferences = getSharedPreferences("history", 0);
        try {
            JSONArray json = new JSONArray(sharedPreferences.getString("history", "[]"));
            json.put(mNewHistoryItemTextView.getText().toString().trim());
            sharedPreferences.edit()
                    .putString("history", json.toString())
                    .apply();
            mNewHistoryItemTextView.setText("");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals("history")) {
            try {
                JSONArray json = new JSONArray(sharedPreferences.getString("history", "[]"));
                List<String> newHistoryItems = new ArrayList<>();
                for (int i = 0; i < json.length(); i++) {
                    newHistoryItems.add(json.optString(i, "I Forgot"));
                }
                this.historyItems = newHistoryItems;
                mAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
